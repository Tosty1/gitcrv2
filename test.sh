#!/usr/bin/env bash


CURRENT_USER=$(whoami)


##################################################################
#
#		Récupération du shell de l'utilisateur
#
##################################################################


CURRENT_SHELL=$(grep ^$CURRENT_USER /etc/passwd | awk -F":" '{print $NF}')

echo "Le script est lancé en tant que $CURRENT_USER avec le shell $CURRENT_SHELL"

##################################################################
#
# 	Si l'utilisateur est root, afficher "Vous êtes administrateur"
#	Sinon, afficher "Vous n'êtes pas administrateur"
#
##################################################################

if [ $CURRENT_USER != "root" ]
then
	echo "Vous n'êtes pas administrateur"
else
        echo "Vous êtes administrateur"
fi

##################################################################
#
#
#	lister la taille des fichiers 
#
#
#################################################################
for f in *; do
	echo "le fichier $f pèse $(stat -c%s $f) octets"
done
